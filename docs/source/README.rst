zopyx.smashdocs
===============

Python wrapper for the Smashdocs Partner API.

Requirements
------------

* Python 2.7
* Python 3.5
* Python 3.6

Smashdocs API version
---------------------

The current version of ``zopyx.smashdocs`` has been tested against
https://documentation.smashdocs.net/release_notes.html#version-1-5-0-0-18-11-2016

Repository
----------

* https://bitbucket.org/ajung/zopyx.smashdocs

Issue tracker
-------------

* https://bitbucket.org/ajung/zopyx.smashdocs/issues?status=new&status=open

License
-------

``zopyx.smashdocs`` is published under the GNU Public License GPL Version 2 (GPL 2).


Author
------
| Andreas Jung/ZOPYX
| Hundskapfklinge 33
| D-72074 Tuebingen, Germany
| info@zopyx.com
| www.zopyx.com


